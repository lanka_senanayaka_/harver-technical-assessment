<?php

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use App\Entity\Department;

class Fixtures implements FixtureInterface
{

    /**
     * Load the Department fixtures
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $Department = new Department(['department_name' => 'test name', 'description' => 'test body']);
        $manager->persist($Department);
        $manager->flush();
    }

    public function getDependencies()
    {
        return array('Test\Fixtures'); // fixture classes fixture is dependent on
    }
}