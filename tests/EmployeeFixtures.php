<?php

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use App\Entity\Employee;

class EmployeeFixtures implements FixtureInterface
{

    /**
     * Load the Department fixtures
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $Department = new Employee(['email' => 'test@test.com', 'employee_name' => 'test name',
            'password' => 'test', 'department_id' => 1]);
        $manager->persist($Department);
        $manager->flush();
    }

    public function getDependencies()
    {
        return array('Test\EmployeeFixtures'); // fixture classes fixture is dependent on
    }
}