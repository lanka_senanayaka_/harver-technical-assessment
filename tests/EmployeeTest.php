<?php

use App\Entity\Employee;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\Loader;
use App\Repositories\EmployeeRepository;
use Fixtures;

class EmployeeTest extends TestCase
{
    private $em;
    private $repository;
    private $loader;

    public function setUp()
    {
        parent::setUp();
        $this->em         = App::make('Doctrine\ORM\EntityManagerInterface');
        $this->repository = new EmployeeRepository($this->em);
        $this->executor   = new ORMExecutor($this->em, new ORMPurger);
        $this->loader     = new Loader;
        $this->loader->addFixture(new Fixtures);
    }

    /** @test */
    public function post($uri, $data = [], $headers = [])
    {
        $purger   = new ORMPurger();
        $executor = new ORMExecutor($this->em, $purger);
        $executor->execute($this->loader->getFixtures());
        $employee = $this->repository->email('test@test.com');
        $this->em->clear();
        $this->assertInstanceOf('App\Entity\EmployeeRepository', $employee);
    }
}