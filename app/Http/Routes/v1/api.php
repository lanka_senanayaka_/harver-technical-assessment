<?php
Route::group(['prefix' => 'api/v1', 'middleware' => 'guest:api'],
    function () {
    Route::get('/departments', 'v1\DepartmentController@index');

    Route::post('/department/create', 'v1\DepartmentController@store');

    Route::get('/department/users/{id}', 'v1\EmployeeController@listUsers');

    Route::get('/employees', 'v1\EmployeeController@index');

    Route::post('/employee/create', 'v1\EmployeeController@store');

    Route::post('/employee/edit/{id}', 'v1\EmployeeController@update');
});
