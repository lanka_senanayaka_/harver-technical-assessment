<?php

namespace App\Http\Controllers\v1;

use App\Repositories\EmployeeRepository as EmployeeRepo;
use App\Validators\EmployeeValidator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;

class EmployeeController extends Controller
{
    private $repo;

    public function __construct(EmployeeRepo $repo)
    {
        $this->repo = $repo;
    }
    /*
     * List all employees
     */

    public function index()
    {
        $departments = $this->repo->retrieve();
        return response()->json($departments);
    }
    /*
     * Create employee
     */

    public function store(Request $request)
    {
        $validator = EmployeeValidator::validate($request->all());

        if ($validator->fails()) {
            return $this->validationErrors($validator);
        }
        $data             = $request->all();
        $data['password'] = bcrypt($data['password']);
        if ($this->repo->create($this->repo->prepareData($data))) {
            /**
             * User created successfully
             */
            return response()->json([
                    'status' => 'success',
                    'message' => 'Employee created.'
            ]);
        } else {
            return response()->json([
                    'status' => 'error',
                    'message' => 'Employee not created.'
            ]);
        }
    }
    /*
     * Update employee
     */

    public function update(Request $request, $id)
    {
        $email     = $this->repo->getEmployee($id);
        $validator = EmployeeValidator::validateUpdate($request->all(), $email);

        if ($validator->fails()) {
            return $this->validationErrors($validator);
        }

        $data             = $request->all();
        $data['password'] = bcrypt($data['password']);

        if ($this->repo->create($this->repo->prepareData($data))) {
            /**
             * User created successfully
             */
            return response()->json([
                    'status' => 'success',
                    'message' => 'Employee updated.'
            ]);
        } else {
            return response()->json([
                    'status' => 'error',
                    'message' => 'Employee not updated.'
            ]);
        }
    }

    public function listUsers($id)
    {
        $departments = $this->repo->listUsers($id);
        return response()->json($departments);
    }

    /**
     * Returns validation errors for department create/edit
     *
     * @param $validator
     * @return mixed
     */
    private function validationErrors($validator)
    {
        $response = [
            'status' => 'error',
            'message' => 'Invalid input data.'
        ];

        foreach ($validator->errors()->all() as $error) {
            $response['details'][] = $error;
        }

        return response()->json($response);
    }
}