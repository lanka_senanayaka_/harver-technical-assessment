<?php

namespace App\Http\Controllers\v1;

use App\Repositories\DepartmentRepository as DepartmentRepo;
use App\Validators\DepartmentValidator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;

class DepartmentController extends Controller
{
    private $repo;

    public function __construct(DepartmentRepo $repo)
    {
        $this->repo = $repo;
    }
    /*
     * List all departments in hierarchical (tree) structure
     */

    public function index()
    {
        $departments = $this->repo->retrieve();
        return response()->json($departments);
    }
    /*
     * Create departments
     */

    public function store(Request $request)
    {
        $validator = DepartmentValidator::validate($request->all());

        if ($validator->fails()) {
            return $this->validationErrors($validator);
        }

        if ($this->repo->create($this->repo->prepareData($request->all()))) {
            /**
             * User created successfully
             */
            return response()->json([
                    'status' => 'success',
                    'message' => 'Department created.'
            ]);
        } else {
            return response()->json([
                    'status' => 'error',
                    'message' => 'Department not created.'
            ]);
        }
    }

    /**
     * Returns validation errors for department create/edit
     *
     * @param $validator
     * @return mixed
     */
    private function validationErrors($validator)
    {
        $response = [
            'status' => 'error',
            'message' => 'Invalid input data.'
        ];

        foreach ($validator->errors()->all() as $error) {
            $response['details'][] = $error;
        }

        return response()->json($response);
    }
}