<?php
/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('/', function () {
    return view('welcome');
});

$route_files_v1 = glob(__DIR__.'/Routes/v1/*.php');

foreach ($route_files_v1 as $partial) {
    if (file_exists($partial)) {
        require_once $partial;
    }
}