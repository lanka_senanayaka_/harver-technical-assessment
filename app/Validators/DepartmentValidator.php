<?php

namespace App\Validators;

use Validator;

class DepartmentValidator
{

    public static function validate($input)
    {
        $rules = [
            'department_name' => 'required|unique:App\Entity\Department',
            'description' => 'required',
            'parent_id' => 'integer',
        ];
        return Validator::make($input, $rules);
    }
}