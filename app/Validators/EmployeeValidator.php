<?php

namespace App\Validators;

use Validator;

class EmployeeValidator
{

    public static function validate($input)
    { //`email`, `password`, `employee_name`, `department_id`
        $rules = [
            'email' => 'email|required|unique:App\Entity\Employee',
            'employee_name' => 'required',
            'password' => 'required|min:3',
            'department_id' => 'required|integer',
        ];
        return Validator::make($input, $rules);
    }

    public static function validateUpdate($input, $email)
    { //`email`, `password`, `employee_name`, `department_id` 
        $rules = [
            'email' => 'email|required|unique:App\Entity\Employee,email,'.$email,
            'employee_name' => 'required',
            'password' => 'required|min:3',
            'department_id' => 'required|integer',
        ];
        return Validator::make($input, $rules);
    }
}