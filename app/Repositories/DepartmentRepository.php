<?php

namespace App\Repositories;

use App\Entity\Department;
use Doctrine\ORM\EntityManager;

class DepartmentRepository
{
    /**
     * @var string
     */
    private $class = 'App\Entity\Department';

    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    /*
     * Get Department list
     */

    public function retrieve()
    {
        $data     = array();
        $elements = $this->em->getRepository($this->class)->findBy(['parent_id' => 0]);
        //dd($elements);
        if (count($elements) > 0) {
            foreach ($elements as $element) {
                $data[] = array(
                    "id" => $element->getId(),
                    "description" => $element->getDescription(),
                    "department_name" => $element->getDepartmentName(),
                    "child_elements" => $this->getChildDepartments($element->getId()),
                );
            }
        }
        return $data;
    }
    /*
     * Create Department
     */

    public function create(Department $post)
    {
        try {
            $this->em->persist($post);
            $this->em->flush();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * create Department
     * @return Department
     */
    public function prepareData($data)
    {
        return new Department($data);
    }

    public function departmentName($department_name)
    {
        return $this->em->getRepository($this->class)->findOneBy([
                'department_name' => $department_name
        ]);
    }

    private function getChildDepartments($id)
    {
        $data     = array();
        $elements = $this->em->getRepository($this->class)->findBy([
            'parent_id' => $id
        ]);

        if (count($elements) > 0) {
            foreach ($elements as $element) {
                $data[] = array(
                    "id" => $element->getId(),
                    "description" => $element->getDescription(),
                    "department_name" => $element->getDepartmentName(),
                    "child_elements" => $this->getChildDepartments($element->getId()),
                );
            }
        }

        return $data;
    }
}