<?php namespace App\Repositories;

use App\Entity\Employee;
use Doctrine\ORM\EntityManager;

class EmployeeRepository
{
    /**
     * @var string
     */
    private $class = 'App\Entity\Employee';
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    
    /*
     * Get Employee list
     */
    public function retrieve(){
        $data=array();
        $elements=$this->em->getRepository($this->class)->findAll();
        //dd($elements);
        if(count($elements)>0){
            foreach ($elements as $element) {
               $data[]=array(
                   "id"=>$element->getId(),
                   "email"=>$element->getEmail(),
                   "employee_name"=>$element->getEmployeeName(),
                   "department"=>$element->getDepartmentId(),
               ); 
            }   
        }
        return $data;

    }
    
    public function email($email)
    {
        return $this->em->getRepository($this->class)->findOneBy([
            'email' => $email
        ]);
    }
    
    public function listUsers($id){
        $data=array();
        $elements=$this->em->getRepository($this->class)->findBy(['department_id'=>$id]);
        //dd($elements);
        if(count($elements)>0){
            foreach ($elements as $element) {
               $data[]=array(
                   "id"=>$element->getId(),
                   "email"=>$element->getEmail(),
                   "employee_name"=>$element->getEmployeeName(),
                   "department"=>$element->getDepartmentId(),
               ); 
            }   
        }
        return $data;

    }

    /*
     * Create Employee
     */
    public function create(Employee $post)
    {
        try{
            $this->em->persist($post);
            $this->em->flush();
            return true;
        }
        catch (Exception $e){
            return false;
        }
        
    }

    
    /**
     * create Department
     * @return Department
     */
    public function prepareData($data)
    {
        return new Employee($data);
    }
    
    public function getEmployee($id){
        $email='';
        $elements= $this->em->getRepository($this->class)->findBy([
            'id' => $id
        ]);
        
        if(count($elements)>0){
            foreach ($elements as $element) {
               $email=$element->getEmail(); 
            }
        }
        
        return $email;
    }
}
