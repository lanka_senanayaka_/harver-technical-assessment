<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="employees")
 * @ORM\HasLifecycleCallbacks()
 */
class Employee
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\OneToOne(targetEntity="App\Entity\Department")
     * @ORM\JoinColumn(name="department_id", referencedColumnName="id")
     */
    private $department_id;

    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string")
     */
    private $employee_name;

    ///**
    // * @ORM\Column(type="integer")
    //  */
    // private $department_id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    public function __construct($input)
    {
        $this->setEmail($input['email']);
        $this->setEmployeeName($input['employee_name']);
        $this->setPassword($input['password']);
        $this->setDepartmentId($input['department_id']);
    }

    public function getId()
    {
        return $this->id;
    }

    public function setEmail($email)
    {
        return $this->email = $email;
    }

    public function setEmployeeName($employee_name)
    {
        $this->employee_name = $employee_name;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function setDepartmentId($department_id)
    {
        $this->department_id = $department_id;
    }

    public function setCreatedAt()
    {
        $this->created_at = new \DateTime("now");
    }

    public function setUpdatedAt()
    {
        $this->updated_at = new \DateTime("now");
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getEmployeeName()
    {
        return $this->employee_name;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getDepartmentId()
    {
        return $this->department_id;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }
}