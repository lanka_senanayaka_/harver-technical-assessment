<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="departments")
 * @ORM\HasLifecycleCallbacks()
 */
class Department
{
    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $department_name;

    /**
     * @ORM\Column(type="string")
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $parent_id = "0";

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    ///**
    // * @ORM\OneToOne(targetEntity="App\Entity\Employee", mappedBy="department")
    // */
//    private $employee;

    public function __construct($input)
    {
        $this->setDepartmentName($input['department_name']);
        $this->setDescription($input['description']);
        $this->parentId($input);
    }

    public function getId()
    {
        return $this->id;
    }

    public function setDepartmentName($department_name)
    {
        return $this->department_name = $department_name;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function setParentId($parent_id)
    {
        $this->parent_id = $parent_id;
    }

    public function setCreatedAt()
    {
        $this->created_at = new \DateTime("now");
    }

    public function setUpdatedAt()
    {
        $this->updated_at = new \DateTime("now");
    }

    public function getDepartmentName()
    {
        return $this->department_name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getParentId()
    {
        return $this->parent_id;
    }

    public function getCreatedAt()
    {
        return $this->created_at;
    }

    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    private function parentId($input)
    {
        if (isset($input['parent_id'])) {
            $this->setParentId($input['parent_id']);
        } else {
            $this->setParentId(0);
        }
    }
}